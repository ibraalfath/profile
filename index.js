//event pada saat link di klik
$(".scroll").on("click", function(e){
    //ambil isi href
    var href = $(this).attr("href");
    //tangkap elemen ybs
    var elemen = $(href);
    
    //pindahkan scroll
    $("html, body").animate({
        scrollTop: elemen.offset().top - 50                                 
    }, 1250, 'easeInOutExpo');

    e.preventDefault();
});
